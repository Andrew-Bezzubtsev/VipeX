#ifndef VIPEX_MM_H
#define VIPEX_MM_H

#include <vipex/semaphore.h>

typedef size_t mm_ptr_t;

#define MM_PAGE_SIZE             0x1000 /* Page size - 4K */
#define MM_PAGE_OFFSET_SIZE      0xC
#define MM_PAGE_OFFSET_MASK      0xFFF
#define MM_PAGE_TABLE_INDEX_SIZE 0xA
#define MM_PAGE_TABLE_INDEX_MASK 0x3FF
#define MM_PHYADDR_SIZE          0xC0

#define MM_PAGE_PRESENT          0x0
#define MM_PAGE_WRITABLE         0x1
#define MM_PAGE_USER             0x2
#define MM_PAGE_WRITE_THROUGH    0x4
#define MM_PAGE_CACHE_DISABLED   0x8
#define MM_PAGE_ACCESSED         0x10
#define MM_PAGE_MODIFIED         0x20
#define MM_PAGE_GLOBAL           0x40

#define MM_KERNEL_BASE           0xFFC00000
#define MM_KERNEL_PAGE_TABLE     0xFFFFE000
#define MM_TEMP_PAGE             0xFFFFF000

#define MM_TEMP_PAGE_INFO (MM_KERNEL_PAGE_TABLE + ((MM_TMP_PAGE >> MM_PAGE_OFFSET_BITS) & MM_PAGE_TABLE_INDEX_MASK) * sizeof(mm_ptr_t))

#define MM_USER_MEMORY_START      ((void *)0)
#define MM_USER_MEMORY_END        ((void *)0x7FFFFFFF)
#define MM_KERNEL_MEMORY_START    ((void *)0x80000000)
#define MM_KERNEL_MEMORY_END      ((void *)(MM_KERNEL_BASE - 1))

enum virtual_memory_block_type_t {
	VMB_T_RESERVED,
	VMB_T_MEMORY,
	VMB_T_IO_MEMORY
};

struct virtual_memory_block_t {
	enum virtual_memory_block_type_t type;
	void *begin;
	size_t length;
};

struct address_space_t {
	mm_ptr_t page_dir;
	void *begin;
	void *end;
	size_t blocks_amount;
	virtual_memory_block_t *blocks;
	vipex_semaphore_t semaphore;
};

mm_ptr_t get_kernel_page_dir(void);

void mm_init(void *addr);
void mm_tmp_map_page(mm_ptr_t addr);
int  mm_map_pages(mm_ptr_t page_dir, void *vaddr, mm_ptr_t paddr, 
                  size_t length, unsigned flags);
mm_ptr_t mm_get_page_info(mm_ptr_t base, void *vaddr);

size_t mm_get_free_memory_amount();
mm_ptr_t mm_allocate_phys_pages(size_t count);
void free_phys_pages(mm_ptr_t base, size_t count);

void *mm_allocate_virtual_pages(struct address_space_t *address_space, 
                                void *vaddr, mm_ptr_t paddr, 
				size_t amount, unsigned flags);
int mm_free_virtual_pages(struct address_space_t *address_space, void *vaddr,
                           size_t amount, unsigned flags);

#endif /* VIPEX_MM_H */
