#ifndef VIPEX_KERNEL_H
#define VIPEX_KERNEL_H

void KERNEL_BASE(void);
void KERNEL_CODE_BASE(void);
void KERNEL_DATA_BASE(void);
void KERNEL_BSS_BASE(void);
void KERNEL_END(void);

int printk(const char * const fmt, ...);

void panic(const char *buf);
void warn(const char *buf);
void report(const char *buf);

#endif // VIPEX_KERNEL_H
