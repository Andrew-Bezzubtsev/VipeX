#ifndef VIPEX_SEMAPHORE_H
#define VIPEX_SEMAPHORE_H

typedef int vipex_semaphore_t;

int semaphore_try_close(vipex_semaphore_t *semaphore, int wait);
void semaphore_open(vipex_semaphore_t *semaphore);

#endif /* VIPEX_SEMAPHORE_H */
