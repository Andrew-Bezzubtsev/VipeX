#ifndef VIPEX_INTERRUPTS_H
#define VIPEX_INTERRUPTS_H

#include <std/stdint.h>

#define IRQ_HANDLER(name) \
void name(void);\
\
asm(\
	#name ": pusha\n"\
	      "  call _" #name "\n"\
	      "  movb $0x20, %al\n"\
	      "  outb %al, $0x20\n"\
	      "  outb %al, $0xA0\n"\
	      "  popa\n"\
	      "  iret\n"\
);\
\
void _ ##name(void)

struct __attribute__((packed)) int_desc {
	uint16_t addr0;
	uint16_t selector;
	uint8_t reserved;
	uint8_t type;
	uint16_t addr1;
};

struct __attribute__((packed)) idtr {
	uint16_t limit;
	void *base;
};


struct vipex_registers_t {
	uint32_t gs, fs, es, ds;
	uint32_t ebp, edi, esi, edx, ecx, ebx, eax;
	uint32_t eip, cs;
	uint32_t eflags;
	uint32_t esp, ss;
};


void interrupts_init(void);
void interrupts_set_int_handler(uint8_t id, void *handler, uint8_t type);
uint8_t interrupts_get_irq_base(void);

#endif // VIPEX_INTERRUPTS_H
