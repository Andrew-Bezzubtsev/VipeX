#ifndef VIPEX_MODULE_H
#define VIPEX_MODULE_H

#include <std/stdint.h>

struct vipexModuleVersion {
	unsigned major;
	unsigned minor;
	int alpha;
};

struct vipexModuleInfo {
	size_t moduleID;
	char *moduleName;
	void (*initFunction)(void);

	struct vipexModuleVersion moduleVersion;
};

struct vipexModuleInfo *vipexAddOSModule(const char *moduleName,
                                         struct vipexModuleVersion ver,
			 		 void (*init)(void));
void vipexDeleteOSModule(struct *vipexModuleInfo moduleInfo);

#endif /* VIPEX_MODULE_H */
