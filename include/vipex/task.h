#ifndef VIPEX_TASK_H
#define VIPEX_TASK_H

#include <vipex/list.h>
#include <vipex/mm.h>

#define VIPEX_PROCESS_NAME_LENGTH_MAX 256
#define VIPEX_PROCESS_NOT_SUSPENDED     0
#define VIPEX_PROCESS_SUSPENDED         1

struct vipex_registers_snapshot_t {
	uint32_t gs;
	uint32_t fs;
	uint32_t es; 
	uint32_t ds;
	uint32_t ebp; 
	uint32_t edi;
	uint32_t esi; 
	uint32_t edx;
	uint32_t ecx;
       	uint32_t ebx;
       	uint32_t eax;
	uint32_t eip;
       	uint32_t cs;
       	uint32_t eflags;
	uint32_t esp;
	uint32_t ss;
};

struct vipex_process_t {
	struct address_space_t address_space;
	int suspended;
	size_t children;
	char name[VIPEX_PROCESS_NAME_LENGTH_MAX];

	struct vipex_process_t *parent;

	void *stack_base;
	size_t stack_size;
	void *stack_pointer;

	struct vipex_registers_snapshot_t saved_registers_state;

	vipex_list_item_t list_item;
};

void multitasking_init(void);
void multitasking_switch_task(struct vipex_registers_t *regs_state);
struct vipex_process_t *vipex_create_process(struct vipex_process_t *parent,
                                             void *entry_point,
					     size_t stack_size,
					     int kernel,
					     int suspended);

#endif /* VIPEX_TASK_H */
