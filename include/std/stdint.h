#ifndef STD_STDINT_H
#define STD_STDINT_H

typedef unsigned char uint8_t;
typedef signed char int8_t;

typedef short unsigned uint16_t;
typedef short signed int16_t;

typedef long unsigned uint32_t;
typedef long signed int32_t;

typedef uint8_t byte;
typedef uint16_t word;
typedef uint32_t dword;

#if defined(__x86_64__)
typedef long long unsigned uint64_t;
typedef long long signed int64_t;
typedef uint64_t size_t;
typedef uint64_t qword;
#else
typedef uint32_t size_t;
#endif

#endif // STD_STDINT_H
