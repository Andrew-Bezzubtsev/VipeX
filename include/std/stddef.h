#ifndef STD_STDDEF_H
#define STD_STDDEF_H

#include <std/stdint.h>

#define NULL ((void *)0)

typedef uint64_t size_t;
typedef int64_t ssize_t;

#endif /* STD_STDDEF_H */
