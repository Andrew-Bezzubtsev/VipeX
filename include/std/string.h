#ifndef STD_STRING_H
#define STD_STRING_H

#include <std/stdint.h>

void *memset(void *m0, byte bv, size_t len);
void *memset_w(void *m0, word wv, size_t len);
void *memset_d(void *m0, dword dv, size_t len);
void *memcpy(void *dst, const void *src, size_t len);
int memcmp(const void *m0, const void *m1, size_t len);
void *memchr(const void *mem, byte bv, size_t len);

size_t strlen(const char *str);
char *strcpy(char *dst, const char *src);
char *strncpy(char *dst, const char *src, size_t len);
int strcmp(const char *s0, const char *s1);
char *strchr(char *str, char val);

#endif // STD_STRING_H
