#ifndef STD_STDARG_H
#define STD_STDARG_H

#define INTSIZEOF(n) ((sizeof(n) + sizeof(int) - 1) & ~(sizeof(int) - 1))

typedef char *va_list;

#define va_start(list, last) \
(list = (va_list)(&last + INTSIZEOF(last)))

#define va_arg(list, type) \
(*(type *)((list += INTSIZEOF(type)) - INTSIZEOF(type)))

#define va_end(list)\
(list = (va_list)0)

#define init_va_list(last) \
((va_list)(&last + INTSIZEOF(last)))

#endif // STD_STDARG_H
