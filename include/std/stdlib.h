#ifndef STD_STDLIB_H
#define STD_STDLIB_H

double atof(const char *str);
int atoi(const char *str);
long atol(const char *str);
long long atoll(const char *str);
double strtod(const char *str, char **endptr);
long strtol(const char *str, char **endptr);

void *malloc(size_t sz);
void *calloc(size_t amount, size_t element_sz);
void *realloc(void *old, size_t new_sz);
void free(void *mem);

char *itoa(int val, char *str, int base);
char *ftoa(double val, char *str, int base);

void memset(void *mem, char unsigned val, size_t count);
void memsetw(void *mem, short unsigned val, size_t count);
void memsetd(void *mem, long unsigned val, size_t count);

void memcpy(void *dest, const void *src, size_t count);
void memcmp(const void *mem0, const void *mem1, size_t count);
void memchr(const void *mem, char unsigned find, size_t count);

#endif // STD_STDLIB_H
