#ifndef STD_STDIO_H
#define STD_STDIO_H

#include <std/stdarg.h>

int sprintf(char *str, const char *fmt, ...);
int vsprintf(char *str, const char *fmt, va_list args);

#endif // STD_STDIO_H
