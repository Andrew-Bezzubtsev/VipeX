#ifndef DRIVERS_TTY_H
#define DRIVERS_TTY_H

#include <std/stdint.h>

void tty_init(void);
void tty_printc(char ch);
void tty_prints(const char *str);
char tty_readc(int wait);
void tty_reads(char *buf, size_t size);
void tty_clear(void);
uint16_t tty_get_width(void);
uint16_t tty_get_height(void);
void tty_set_attribute(uint8_t val);
uint8_t tty_get_attribute(void);
void tty_set_cursor_pos(uint16_t pos);
uint16_t tty_get_cursor_pos(void);

#endif // DRIVERS_TTY_H
