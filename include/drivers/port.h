#ifndef DRIVERS_PORT_H
#define DRIVERS_PORT_H

#include <std/stdint.h>

#define port_outb(port, val) \
asm(\
	"outb %b0, %w1"\
	:\
	:"a"(val),\
	 "d"(port)\
);

#define port_outw(port, val) \
asm(\
	"outw %w0, %w1"\
	:\
	:"a"(val),\
	 "d"(port)\
);

#define port_outd(port, val) \
asm(\
	"outl %0, %w1"\
	:\
	:"a"(val),\
	 "d"(port)\
);

#define port_inb(port) \
({\
	byte val = 0;\
	asm(\
		"inb %w1, %b0"\
		:"=a"(val)\
		:"d"(port)\
	);\
	val;\
})

#define port_inw(port) \
({\
	word val = 0;\
	asm(\
		"inw %w1, %w0"\
		:"=a"(val)\
		:"d"(port)\
	);\
	val;\
})

#define port_ind(port) \
({\
	dword val = 0;\
	asm(\
		"inl, %w1, %0"\
		:"=a"(val)\
		:"d"(port)\
	);\
	val;\
})

#endif // DRIVERS_PORT_H
