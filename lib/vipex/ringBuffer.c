#include <std/stdint.h>
#include <std/stdlib.h>

struct vipexRingBuffer *vipexRingBufferInit(void)
{
	struct vipexRingBuffer *res = (struct vipexRingBuffer *)calloc(1, sizeof(struct vipexRingBuffer));

	res->mem = 0;
	res->sz = 0;

	return res;
}

void vipexRingBufferAddMember(struct vipexRingBuffer *buffer, void *val)
{
	if (!buffer)
		vipexRaiseException(VIPEX_RINGBUFFER_EXCEPTION_NULL_PTR);

	buffer->mem = realloc(buffer->mem, buffer->sz++);
	*(void **)(buffer->mem + buffer->sz * sizeof(void *)) = val;
}

void vipexRingBufferDeleteMember(struct vipexRingBuffer *buffer, size_t pos)
{
	if (!buffer)
		vipexRaiseException(VIPEX_RINGBUFFER_EXCEPTION_NULL_PTR);

	pos %= buffer->sz;

	for (size_t i = pos; i < buffer->pos - 1; i++)
		((void **)buffer->mem)[i] = ((void **)(buffer->mem))[i + 1];

	buffer->mem = realloc(buffer->mem, buffer->sz--);
}

void *vipexRingBufferGetMemberValue(struct vipexRingBuffer *buffer, size_t pos)
{
	if (!buffer)
		VIPEX_RINGBUFFER_EXCEPTION_NULL_PTR

	pos %= buffer->sz;
	return ((void **)buffer->mem)[pos];
}

void vipexRingBufferDelete(struct vipexRingBuffer *buffer)
{
	free(buffer->mem);
	free(buffer);
}
