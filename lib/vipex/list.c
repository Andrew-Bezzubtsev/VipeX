#include <vipex/semaphore.h>
#include <vipex/list.h>

void vipex_list_init(struct vipex_list_t *list)
{
	list->first = NULL;
	list->sz = 0;
	list->semaphore = VIPEX_SEMAPHORE_DISABLED;
}

void vipex_list_insert(struct vipex_list_t *list, struct vipex_list_item_t *item)
{
	if (item->list == NULL && list) {
		semaphore_try_close(&(list->semaphore), VIPEX_SEMAPHORE_WAIT_CLOSE);

		if (list->first) {
			item->list = list;
			item->next = list->first;
			item->prev = list->first->prev;
			item->prev->next = item;
			item->next->prev = item;
		} else {
			item->next = item;
			item->prev = item;
			list->first = item;
		}

		list->sz++;

		semaphore_open(&(list->semaphore));
	}
}

void vipex_list_remove(struct vipex_list_item_t *item)
{
	semaphore_try_close(&(item->list->semaphore), VIPEX_SEMAPHORE_WAIT_CLOSE);

	if (item->list && item->list->sz) {
		if (item->list->first == item) {
			item->list->first = item->next;

			if (item->list->first == item)
				item->list->first = NULL;
		}

		item->next->prev = item->prev;
		item->prev->next = item->next;
		item->list->sz--;
	}

	semaphore_open(&(item->list->semaphore));
}
