#include <vipex/mm.h>
#include <vipex/kernel.h>
#include <vipex/semaphore.h>

#define TMP_PAGE ((volatile struct physical_memory_block_t *)MM_TEMP_PAGE)

struct __attribute__((packed)) mm_mem_map_entry {
	mm_ptr_t base;
	size_t length;
	uint32_t type;
	uint32_t acpi_ext_attributes;
};

struct physical_memory_block_t {
	mm_ptr_t next;
	mm_ptr_t prev;
	size_t sz;
};

static size_t __mm_free_pages_amount;
static mm_ptr_t __mm_free_mem_ptr;
static mm_ptr_t __mm_kernel_page_dir;
static size_t __mm_memory_size;
static address_space_t __mm_kernel_address_space;
static vipex_semaphore_t __mm_semaphore;

static inline void __mm_flush_page_cache(void *ptr)
{
	asm(
		"invlpg (,%0,)"
		:
		:"a"(ptr)
	);
}

void mm_init(void *addr)
{
	struct mm_mem_map_entry *entry;

	asm(
		"movl %%cr3, %0"
		:"=a"(__mm_kernel_page_dir)
	);
	
	__mm_memory_size = 0x100000;
	__mm_free_pages_amount = 0;
	__mm_free_mem_ptr = 0;

	for (entry = memory_map; entry->type; entry++) {
		if ((entry->type == 1) && (entry->base >= 0x100000)) {
			free_phys_pages(entry->base, entry->length >> MM_PAGE_OFFSET_SIZE);
			memory_size += entry->length;
		}
	}

	mm_map_pages(__mm_kernel_page_dir, KERNEL_CODE_BASE, mm_get_page_info(__mm_kernel_page_dir, KERNEL_CODE_BASE),
	             ((size_t)KERNEL_DATA_BASE - (size_t)KERNEL_CODE_BASE) >> MM_PAGE_OFFSET_SIZE, 
		     MM_PAGE_PRESENT | MM_PAGE_GLOBAL);
	mm_map_pages(__mm_kernel_page_dir, KERNEL_DATA_BASE, mm_get_page_info(__mm_kernel_page_dir, KERNEL_DATA_BASE),
	             ((size_t)KERNEL_END - (size_t)KERNEL_DATA_BASE) >> MM_PAGE_OFFSET_SIZE,
		     MM_PAGE_PRESENT | MM_PAGE_WRITABLE | MM_PAGE_GLOBAL);
	mm_map_pages(__mm_kernel_page_dir, KERNEL_PAGE_TABLE, mm_get_page_info(__mm_kernel_page_dir, KERNEL_PAGE_TABLE),
	             1, MM_PAGE_PRESENT | MM_PAGE_WRITABLE | MM_PAGE_GLOBAL);

	__mm_kernel_address_space.page_dir = __mm_kernel_page_dir;
	__mm_kernel_address_space.begin = MM_KERNEL_MEMORY_START;
	__mm_kernel_address_space.end = MM_KERNEL_MEMORY_END;
	__mm_kernel_address_space.block_table_size = MM_PAGE_SIZE / sizeof(struct virtual_memory_block_t);
	__mm_kernel_address_space.blocks = MM_KERNEL_MEMORY_START;
	__mm_kernel_address_space.blocks_amount = 1;

	mm_map_pages(__mm_kernel_page_dir, __mm_kernel_address_space.blocks,
	             mm_allocate_phys_pages(1), 1,
		     MM_PAGE_PRESENT | MM_PAGE_WRITABLE);

	__mm_kernel_address_space.blocks->type = VMB_T_RESERVED;
	__mm_kernel_address_space.blocks->base = __mm_kernel_address_space.blocks;
	__mm_kernel_address_space.blocks->length = MM_PAGE_SIZE;
}

void mm_tmp_map_page(mm_ptr_t addr)
{
	((mm_ptr_t *)MM_TEMP_PAGE_INFO) = (addr & ~MM_PAGE_OFFSET_MASK) | MM_PAGE_VALID | MM_PAGE_PRESENT | MM_PAGE_WRITABLE;

	__mm_flush_page_cache((void *)MM_TEMP_PAGE);
}

int mm_map_pages(mm_ptr_t page_dir, void *vaddr, mm_ptr_t paddr,
		 size_t length, unsigned flags)
{
	while (length) {
		mm_ptr_t page_table = page_dir;
		char offset;

		for (offset = MM_PHYADDR_SIZE - MM_PAGE_TABLE_INDEX_SIZE;
		     offset >= MM_PAGE_OFFSET_SIZE;
		     offset -= MM_PAGE_TABLE_INDEX_SIZE) {
			unsigned id = ((size_t)vaddr >> offset) & 
			              MM_PAGE_TABLE_INDEX_MASK;

			mm_tmp_map_page(page_table);

			if (offset > MM_PAGE_OFFSET_SIZE) {
				page_table = ((volatile mm_ptr_t *)MM_TMP_PAGE)[id];

				if (!(page_table & MM_PAGE_PRESENT)) {
					mm_ptr_t page = mm_allocate_phys_pages(1);

					if (page) {
						mm_tmp_map_page(page);
						memset((void *)page, 0, MM_PAGE_SIZE);
						mm_tmp_map_page(page_table);
						((mm_ptr_t *)MM_TMP_PAGE)[id] = page | MM_PAGE_PRESENT | MM_PAGE_VALID | MM_PAGE_WRITABLE | MM_PAGE_USER;
						page_table = page;
					} else {
						return 1;
					}
				} 
			} else {
				((volatile mm_ptr_t *)MM_TMP_PAGE)[id] = (paddr & ~ MM_PAGE_OFFSET_BITS) | flags;
				__mm_flush_page_cache(vaddr);
			}
			
			vaddr += MM_PAGE_SIZE;
			paddr += MM_PAGE_SIZE;
		}

		length--;
	}

	return 0;
}

mm_ptr_t mm_get_page_info(mm_ptr_t base, void *vaddr)
{
	mm_ptr_t table = base;
	char offset;

	for (offset = MM_PHYADDR_SIZE - MM_PAGE_TABLE_INDEX_SIZE;
	     offset >= MM_PAGE_OFFSET_BITS;
	     offset -= MM_PAGE_TABLE_INDEX_SIZE) {
		unsigned id = ((size_t)vaddr >> offset) & MM_PAGE_TABLE_INDEX_MASK;
		mm_tmp_map_page(table);

		if (offset > MM_PAGE_OFFSET_BITS) {
			table = ((volatile mm_ptr_t *)MM_TMP_PAGE)[id];

			if (!(table & MM_PAGE_PRESENT))
				return 0;
		} else {
			return ((volatile mm_ptr_t *)MM_TMP_PAGE)[id];
		}
	}

	return 0;
}

size_t mm_get_free_memory_amount()
{
	return 0;
}

mm_ptr_t mm_allocate_phys_pages(size_t count)
{
	if (__mm_free_pages_amount < count)
		return (mm_ptr_t)-1;

	mm_ptr_t res = (mm_ptr_t)-1;
	semaphore_try_close(&__mm_semaphore, 1);

	if (__mm_free_mem_ptr != (mm_ptr_t)-1) {
		mm_ptr_t current_block = __mm_free_mem_ptr;

		do {
			mm_tmp_map_page(current_block);

			if (TMP_PAGE->size == count) {
				mm_ptr_t str_next = TMP_PAGE->next;
				mm_ptr_t str_prev = TMP_PAGE->prev;
				
				mm_tmp_map_page(str_next);
				TMP_PAGE->prev = str_prev;
				mm_tmp_map_page(str_prev);
				TMP_PAGE->next = str_next;

				if (current_block == __mm_free_mem_ptr) {
					__mm_free_mem_ptr = str_next;

					if (current_block == __mm_free_mem_ptr)
						__mm_free_mem_ptr = -1;
				}

				res = current_block;
				break;
			} else if (TMP_PAGE->size > count) {
				TMP_PAGE->size -= count;
				res = current_block + (TMP_PAGE->size << MM_PAGE_OFFSET_SIZE);
				break;
			}

			current_block = tmp_page->next;
		} while (current_block != __mm_free_mem_ptr);

		if (res != (mm_ptr_t)-1)
			__mm_free_pages_amount -= count;	
	}

	semaphore_open(&__mm_semaphore);

	return res;
}

void mm_free_phys_pages(mm_ptr_t base, size_t count)
{
	semaphore_try_close(&__mm_semaphore, 1);

	if (__mm_free_mem_ptr == (mm_ptr_t)-1) {
		mm_tmp_map_page(base);

		TMP_PAGE->next = base;
		TMP_PAGE->prev = base;
		TMP_PAGE->size = count;

		__mm_free_mem_ptr = base;
	} else {
		mm_ptr_t current_block = __mm_free_mem_ptr;

		do {
			mm_tmp_map_page(current_block);

			if (current_block + (TMP_PAGE->size << MM_PAGE_OFFSET_SIZE) == base) {
				TMP_PAGE->size += count;

				if (TMP_PAGE->next == base + (count << MM_PAGE_OFFSET_SIZE)) {
					mm_ptr_t nxt[2] = {TMP_PAGE->next};
					size_t new_count;

					mm_tmp_map_page(nxt[0]);
					nxt[1] = TMP_PAGE->next;

					new_count = TMP_PAGE->size;
					mm_tmp_map_page(nxt[1]);
					
					TMP_PAGE->prev = current_block;
					mm_tmp_map_page(current_block);
					TMP_PAGE->next = nxt[1];
					TMP_PAGE->size += new_count;
				}

				break;
			} else if (base + (count << MM_PAGE_OFFSET_SIZE) == current_block) {
				size_t old_sz = TMP_PAGE->size;
				mm_ptr_t old_next = TMP_PAGE->next;
				mm_ptr_t old_prev = TMP_PAGE->prev;

				mm_tmp_map_page(old_next);
				TMP_PAGE->prev = base;
				mm_tmp_map_page(old_prev);
				TMP_PAGE->next = base;

				mm_tmp_map_page(base);
				TMP_PAGE->next = old_next;
				TMP_PAGE->prev = old_prev;
				TMP_PAGE->size = old_sz + count;
			
				break;
			} else if ((current_block > base) ||
			           (TMP_PAGE->next == __mm_free_mem_ptr)) {
				mm_ptr_t prev = TMP_PAGE->next;

				TMP_PAGE->prev = base;
				mm_tmp_map_page(prev);
	
				TMP_PAGE->next = base;
				mm_tmp_map_page(base);

				TMP_PAGE->next = current_block;
				TMP_PAGE->prev = prev;
				TMP_PAGE->size = count;

				break;
			}

			current_block = TMP_PAGE->next;
		} while (current_block != __mm_free_mem_ptr);

		if (base < __mm_free_mem_ptr)
			__mm_free_mem_ptr = base;
	}

	__mm_free_pages_amount += count;

	semaphore_open(&__mm_semaphore);
}

static inline int __mm_is_blocks_overlapped(void *base1, size_t size1, void *base2, size_t size2)
{
	return ((base1 >= base2) && (base1 < base2 + size2)) || ((base2 >= base1) && (base2 < base1 + size1));
}

void *mm_allocate_virtual_pages(struct address_space_t *address_space, void *vaddr, mm_ptr_t paddr, size_t amount, unsigned flags)
{	
	enum virtual_memory_block_type_t type = VMB_IO_MEMORY;

	semaphore_try_close(&(address_space->semaphore), 1);

	if (!vaddr) {
		vaddr = address_space->end - (amount << MM_PAGE_OFFSET_SIZE) + 1;
		
		for (size_t i = 0; i < address_space->block_count; i++) {
			if (__mm_is_blocks_overlapped(address_space->blocks[i].base, address_space->blocks[i].length, vaddr, amount))
				vaddr = address_space->blocks[i].base - (count << MM_PAGE_OFFSET_BITS);
			else
				break;
		}
	} else {
		if ((vaddr >= address_space->start) && (vaddr + (count << MM_PAGE_OFFSET_SIZE) < address_space->end)) {
			for (size_t i = 0; i < address_space->block_count; i++) {
				if (is_blocks_overlapped(address_space->blocks[i].base, address_space->blocks[i].length, vaddr, count << PAGE_OFFSET_BITS)) {
					vaddr = NULL;
					break;
				} else if (address_space->blocks[i].base < vaddr) {
					break;
				}
			}
		} else {
			vaddr = NULL;
		}
	}
	
	if (vaddr) {
		if (paddr == -1) {
			paddr = mm_allocate_phys_pages(count);
			type = VMB_T_MEMORY;
		}
		
		if (paddr != -1) {
			if (mm_map_pages(address_space->page_dir, vaddr, paddr, count, flags)) {
				if (address_space->block_count == address_space->block_table_size) {
					size_t new_size = address_space->block_table_size * sizeof(VirtMemoryBlock);
					new_size = (new_size + MM_PAGE_OFFSET_MASK) & ~MM_PAGE_OFFSET_MASK;
					new_size += MM_PAGE_SIZE;
					new_size = new_size >> MM_PAGE_OFFSET_SIZE;
					
					if (&kernel_address_space != address_space) {
						virtual_memory_block_t *new_table = alloc_virt_pages(&kernel_address_space, NULL, -1, new_size,
											             MM_PAGE_PRESENT | MM_PAGE_WRITABLE);
						if (new_table) {
							memcpy(new_table, address_space->blocks, address_space->block_table_size * sizeof(struct virtual_memory_block_t));
							free_virtual_pages(&kernel_address_space, address_space->blocks, 0);
							address_space->blocks = new_table;
						} else {
							goto failure;
						}
					} else {
						mm_ptr_t new_page = alloc_phys_pages(1);
						if (new_page == -1) {
							goto failure;
						} else {
							struct virtual_memory_block_t *main_block = &(address_space->blocks[address_space->block_count - 1]);
							
							if (mm_map_pages(address_space->page_dir, main_block->base + main_block->length, new_page, 1, MM_PAGE_PRESENT | MM_PAGE_WRITABLE))
								main_block->length += MM_PAGE_SIZE;
							else
								free_phys_pages(new_page, 1);
						}
					}
					
					address_space->block_table_size = (new_size << MM_PAGE_OFFSET_SIZE) / sizeof(struct virtual_memory_block_t);
				}
				
				memcpy(address_space->blocks + i + 1, address_space->blocks + i, (address_space->block_count - i) * sizeof(struct virtual_memory_block_t));
				
				address_space->block_count++;
				address_space->blocks[i].type = type;
				address_space->blocks[i].base = vaddr;
				address_space->blocks[i].length = count << PAGE_OFFSET_BITS;
			} else {
failure:
				mm_map_pages(address_space->page_dir, vaddr, 0, count, 0);
				free_phys_pages(paddr, count);
				vaddr = NULL;
			}
		}
	}
	
	semaphore_open(&(address_space->semaphore));

	return vaddr;
}

int mm_free_virt_pages(struct mm_address_space_t *address_space, void *vaddr, unsigned flags)
{
	semaphore_try_close(&(address_space->semaphore));

	for (size_t i = 0; i < address_space->block_count; i++)
		if ((address_space->blocks[i].base <= vaddr) && (address_space->blocks[i].base + address_space->blocks[i].length > vaddr))
			break;
			
	if (i < address_space->block_count) {
		if (address_space->blocks[i].type = VMB_T_MEMORY)
			free_phys_pages(get_page_info(address_space->page_dir, vaddr) & ~MM_PAGE_OFFSET_MASK, address_space->blocks[i].length >> MM_PAGE_OFFSET_SIZE);
			
		address_space->block_count--;
		memcpy(address_space->blocks + i, address_space->blocks + i + 1, (address_space->block_count - i) * sizeof(struct virtual_memory_block_t));

		return 1;
	}
	
	semaphore_open(&(address_space->semaphore));

	return 0;
}

void mm_init_address_space(struct mm_address_space_t *address_space,
                           mm_ptr_t phypage)
{
	address_space->page_dir = phypage;
	address_space->begin = USER_MEMORY_BEGIN;
	address_space->end = USER_MEMORY_END;
	address_space->block_table_size = MM_PAGE_SIZE / sizeof(struct mm_virtual_memory_block_t);
	address_space->blocks = mm_allocate_virtual_pages(mm_get_kernel_address_space(), NULL, -1, 1, MM_PAGE_PRESENT | MM_PAGE_WRITABLE);
	address_space->blocks_amount = 1;
}
