#include <vipex/list.h>
#include <vipex/mm.h>
#include <vipex/interrupts.h>

#include <vipex/task.h>

struct vipex_list_t __multitasking_process_list;
struct vipex_process_t *__multitasking_current_process;
struct vipex_process_t *__multitasking_kernel_process;

IRQ_HANDLER(task_switch_int_handler) {
	asm(
		"movl %esp, %0"
		:"=a"(__multitasking_current_process->stack_pointer)
	);

	do {
		__multitasking_current_process = __multitasking_current_process->list_item.next;
	} while (current_process->suspended);

	asm(
		"movl %0, %cr3"
		:
		:"a"(__multitasking_current_process->address_space.page_dir)
	);

	asm(
		"movl %0, %esp"
		:
		:"a"(__multitasking_current_process->stack_pointer)
	);
}

void multitasking_init(void)
{
	vipex_list_init(&__multitasking_process_list);

	__multitasking_kernel_process = (struct vipex_process_t *)
		mm_allocate_virtual_pages(mm_get_kernel_address_space(), NULL, 
		                          -1, 1, 
					  MM_PAGE_PRESENT | MM_PAGE_WRITABLE);
	mm_init_address_space(&__multitasking_kernel_process->address_space,
	                      mm_get_kernel_page_dir());

	__multitasking_kernel_process->address_space.page_dir = mm_get_kernel_page_dir();
	__multitasking_kernel_process->suspended = VIPEX_PROCESS_NOT_SUSPENDED;
	__multitasking_kernel_process->children = 0;

	strncpy(__multitasking_kernel_process->name,
		"[Kernel process]", VIPEX_PROCESS_NAME_LENGTH_MAX);

	__multitasking_kernel_process->stack_size = MM_PAGE_SIZE;
	vipex_list_insert(__multitasking_process_list, __multitasking_kernel_process);

	__multitasking_current_process = __multitasking_kernel_process;

	interrupts_set_int_handler(irq_base, task_switch_int_handler, 0x8E);
}

void multitasking_switch_task(struct vipex_registers_t *regs_state)
{
	if (__multitasking_enabled) {
		memcpy(&__multitasking_current_process->saved_registers_state,
		       regs_state, sizeof(struct vipex_registers_t));

		do {
			__multitasking_current_process = current_process->list_item.next;
		} while (__multitasking_current_process->suspended);

		asm(
			"movl %0, %%cr3"
			:
			:"a"(__multitasking_current_process->address_space.page_dir)
		);

		memcpy(regs_state, &__multitasking_current_process->saved_registers_state, 
		       sizeof(struct vipex_registers_t));
	}
}

struct vipex_process_t *vipex_create_process(struct vipex_process_t *parent,
                                             void *entry_point,
					     size_t stack_size,
					     int kernel,
					     int suspended)
{
	struct mm_address_space_t *address_space;
	struct vipex_process_t *res;

	if (parent)
		address_space = parent->address_space;
	else
		address_space = mm_get_kernel_address_space();

	res = mm_allocate_virtual_pages(address_space, NULL, -1, 1, 
	                                MM_PAGE_PRESENT | MM_PAGE_WRITABLE);
	res->parent = parent;
	res->suspended = suspended;

	vipex_list_insert(&__multitasking_process_list, res);

	if (parent)
		parent->children++;

	return res;
}
