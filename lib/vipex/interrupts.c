#include <std/stdint.h>
#include <std/string.h>

#include <drivers/port.h>
#include <drivers/tty.h>

#include <vipex/interrupts.h>
#include <vipex/mm.h>
#include <vipex/task.h>

void (*__irq_handlers[])();

void __irq_handler(uint32_t index, struct vipex_registers_t *regs);

struct int_desc *__interrupts_desc; 
uint8_t __irq_base;
uint8_t __irq_count;

IRQ_HANDLER(timer_int_handler) 
{ }

void interrupts_init(void)
{
	mm_map_pages(get_kernel_page_dir(), __interrupts_desc,
	             mm_allocate_phys_pages(1), 1,
		     MM_PAGE_PRESENT | MM_PAGE_WRITABLE | MM_PAGE_GLOBAL);

	memset(__interrupts_desc, 0, 256 * sizeof(struct int_desc));

	volatile struct idtr idtr;
	idtr.limit = 256 * sizeof(struct int_desc);
       	idtr.base = __interrupts_desc;

	asm(
		"lidt (,%0,)"
		:
		:"a"(&idtr)
	);

	__irq_base = 0x20;
	__irq_count = 16;

	port_outb(0x20, 0x11);
	port_outb(0x21, __irq_base);
	port_outb(0x21, 0x4);
	port_outb(0x21, 0x1);

	port_outb(0xA0, 0x11);
	port_outb(0xA1, __irq_base + 8);
	port_outb(0xA1, 0x2);
	port_outb(0xA1, 0x1);

	for (int i = 0; i < 16; i++)
		interrupts_set_int_handler(__irq_base + i, irq_handlers[i], 0x8E);

	asm(
		"sti"
	);
}

void interrupts_set_int_handler(uint8_t id, void *handler, uint8_t type)
{
	size_t saved_flags;
	asm(
		"pushf\n"
		"pop1 %0\n"
		"cli"
		:"=a"(&saved_flags)
	);

	__interrupts_desc[id].selector = 8;
	__interrupts_desc[id].addr0 = (size_t)handler & 0xFFFF;
	__interrupts_desc[id].addr1 = ((size_t)handler >> 16) & 0xFFFF;
	__interrupts_desc[id].type = type;
	__interrupts_desc[id].reserved = 0;

	asm(
		"push1 %0\n"
		"popf"
		:
		:"a"(saved_flags)
	);
}

uint8_t interrupts_get_irq_base(void)
{
	return __irq_base;
}

void __irq_handler(uint32_t id, struct vipex_registers_t *regs)
{
	switch (id) {
		case 0:
			multitasking_switch_task(regs);
			break;

		case 1:
			keyboard_int_handler();
			break;

		default:
			break;
	}
}
