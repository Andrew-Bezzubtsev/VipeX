#include <vipex/semaphore.h>

int semaphore_try_close(vipex_semaphore_t *semaphore, int wait)
{
	int old_val = 1;

	do {
		asm(
			"xchg (,%1,), %0"
			:"=a"(old_val)
			:"d"(semaphore), "a"(old_val)
		);
	} while (old_val && wait);

	return !old_val;
}

void semaphore_open(vipex_semaphore_t *semaphore)
{
	*semaphore = 0;
}
