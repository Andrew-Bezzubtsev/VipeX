#include <std/stdint.h>
#include <vipex/ringBuffer.h>

#include <vipex/module.h>

static struct vipexRingBuffer *__vipexModulesInfo__;

struct vipexModuleInfo *vipexAddOSModule(const char *moduleName,
                                         struct vipexModuleVersion ver,
					 void (*init)(void))
{
	static int runned = 0;
	if (!runned) {
		__vipexModulesInfo__ = 0;
		runned = 1;
	}

	struct vipexModuleInfo *moduleInfo = calloc(1, sizeof(struct vipexModuleInfo));
	moduleInfo->moduleID = (size_t)moduleInfo;
	moduleInfo->moduleName = calloc(strlen(moduleName) + 1, sizeof(char));
	strcpy(moduleInfo->moduleName, moduleName);
	moduleInfo->initFunction = init;
	memcpy(&(moduleInfo->moduleVersion), ver, sizeof(struct vipexModuleVersion));

	vipexRingBufferInsertMember(__vipexModulesInfo__, (void *)moduleInfo);

	return moduleInfo;
}

void vipexDeleteOSModule(struct vipexModuleInfo *moduleInfo)
{
	vipexRingBufferDeleteMember(__vipexModulesInfo__, vipexRingBufferGetMemberID(moduleInfo));
	free((void *)moduleInfo->moduleName);
	free((void *)moduleInfo);
}
