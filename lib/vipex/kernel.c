#include <vipex/kernel.h>
#include <drivers/tty.h>

#include <std/stdarg.h>
#include <std/stdio.h>

int printk(const char *fmt, ...)
{
	char buf[1024] = {};
	va_list args;

	va_start(list, fmt);
	vsprintf(buf, fmt, args);
	va_end(args);

	tty_prints(buf);
}

void panic(const char *buf)
{
	printk("Fatal: %s!", buf);
}

void warn(const char *buf)
{
	printk("Warning: %s!", buf);
}

void report(const char *buf)
{
	printk("Reported: %s!", buf);
}
