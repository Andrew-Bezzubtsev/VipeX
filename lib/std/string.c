#include <std/stdint.h>
#include <std/string.h>
#include <vipex/cmp.h>

void *memset(void *m0, byte bv, size_t len)
{
	asm(
		"movl %0, %%eax\n" 
		"movl %1, %%edi\n"
		"movl %2, %%ecx\n"
		"rep stosl"
		:
		:"a"((uint32_t)bv | 
		    ((uint32_t)bv << 8) | 
		    ((uint32_t)bv << 16) | 
		    ((uint32_t)bv << 24)), 
		 "b"(m0),
		 "c"(len >> 2)
	);

	asm(
		"movb %b0, %%al\n"
		"movl %1, %%ecx\n" 
		"rep stosb"
		:
		:"a"(bv), 
		 "b"(len & 3)
	);

	return m0;
}

void *memset_w(void *m0, word wv, size_t len)
{
	asm(
		"movl %0, %%eax\n"
		"movl %1, %%edi\n"
		"movl %2, %%ecx\n"
		"rep stosl"
		:
		:"a"((uint32_t)wv | 
		    ((uint32_t)wv << 16)),
		 "b"(m0),
		 "c"(len >> 1)
	);

	return m0;
}

void *memset_d(void *m0, dword dv, size_t len)
{
	asm(
		"movl %0, %%eax\n"
		"movl %1, %%edi\n"
		"movl %2, %%ecx\n"
		"rep stosl"
		:
		:"a"(dv),
		 "b"(m0),
		 "c"(len)
	);

	return m0;
}

void *memcpy(void *dst, const void *src, size_t len)
{
	asm(
		"movl %0, %%edi\n"
		"movl %1, %%esi\n" 
		"movl %2, %%ecx\n" 
		"rep movsl"
		:
		:"a"(dst),
		 "b"(src),
		 "c"(len >> 2)
	);

	asm(
		"movl %0, %%ecx\n"
		"rep movsb"
		:
		:"a"(len & 3)
	);

	return dst;
}

int memcmp(const void *m0, const void *m1, size_t len)
{
	char above = 0, 
	     below = 0;

	asm(
		"movl %0, %%esi\n"
	       	"movl %1, %%edi\n" 
		"movl %2, %%ecx\n"
		"repe cmpsb"
		:
		:"a"(m0),
		 "b"(m1),
		 "c"(len)
	);

	asm(
		"seta %0\n"
		"setb %1"
		:"=a"(above),
		 "=b"(below)
	);

	return above - below;
}

void *memchr(const void *mem, uint8_t bv, size_t len)
{
	void *res = 0;

	asm(
		"movb %b0, %%al\n"
		"movl %1, %%edi\n"
		"movl %2, %%ecx\n"
		"repe cmpsb"
		:
		:"a"(bv),
		 "b"(mem),
		 "c"(len)
	);

	asm(
		"movl %%edi, %0"
		:"=a"(res)
	);

	if (res < mem + len)
		return res;

	return 0;
}

size_t strlen(const char *str)
{
	return (size_t)((char *)memchr(str, 0, -1) - str);
}

char *strcpy(char *dst, const char *src)
{
	memcpy(dst, src, strlen(src) + 1);
	return dst;
}

char *strncpy(char *dst, const char *src, size_t len)
{
	size_t sz = MIN(len - 1, strlen(src));
	memcpy(dst, src, sz);

	return dst;
}

int strcmp(const char *s0, const char *s1)
{
	return memcmp(s0, s1, MIN(strlen(s0), strlen(s1)) + 1);
}

char *strchr(char *str, char val)
{
	return memchr(str, val, strlen(str));
}
