#include <std/stdlib.h>
#include <std/string.h>
#include <std/stdint.h>

double atof(const char *str)
{
	if (!str)
		return 0;

	double integer = 0, fraction = 0;
	unsigned fraction_devisor = 1;
	int neg = 0;
	int in_fraction = 0;
	size_t l = strlen(str);

	for (size_t i = 0; i < l; i++) {
		if (str[i] == '-') {
			neg = !neg;
		} else if (isdigit(str[i])) {
			if (in_fraction) {
				fraction = fraction * 10 + str[i] - '0';
				fraction_devisor *= 10;
			} else {
				integer = integer * 10 + str[i] - '0';
			}
		} else if (str[i] == '.') {
			if (in_fraction) {
				double res = integer + fraction / fraction_devisor;
				return (neg)?(-res):(res);
			} else {
				in_fraction = true;
			}
		} else {
			double res = integer + fraction / fraction_devisor;
			return (neg)?(-res):(res);
		}
	}

	double res = integer + fraction / fraction_devisor;
	return (neg)?(-res):(res);
}

#define ATOI_BLOCK(res_t, str) \
{ \
	if (!str) \
		return 0; \
	\
	int neg = 0; \
	res_t res = 0; \
	size_t l = strlen(str); \
	\
	for (size_t i = 0; i < l; i++) { \
		if (str[i] == '-') \
			neg = !neg; \
		else if (isdigit(str[i])) \
			res = res * 10 + str[i] - '0'; \
	} \
	\
	return (neg)?(-res):(res); \
}

int atoi(const char *str)
{
	ATO_BLOCK(int, str)
}

long atol(const char *str)
{
	ATO_BLOCK(long, str)
}

long long atoll(const char *str)
{
	ATO_BLOCK(long long, str)
}

#undef ATOI_BLOCK

double strtod(const char *str, char **endptr)
{
	/* Do this later... */
}

long strtol(const char *str, char **endptr)
{
	/* Do this later.. */
}

void *malloc(size_t sz)
{
	if (!__grabbed_blocks->nentries) {
		struct __page_list_entry_t *new_list_entry;
		new_list_entry = (struct __page_list_entry_t *)(mm_allocate_phys_pages(1) * MM_PAGE_SIZE);
		new_list_entry->
	return res;

no_mem:
	errno = ENOMEM;
	return NULL;
}

void *calloc(size_t amount, size_t element_sz)
{
	size_t sz = amount * element_sz;
	void *res = malloc(sz);

	memset(res, 0, sz);

	return res;
}

void *realloc(void *old, size_t new_sz)
{
	void *new_mem = malloc(new_sz);
	memcpy(new_mem, old, new_sz);

	free(old);
	return new_mem;
}

void free(void *mem)
{
	/* Do this later... */
}

char *itoa(int val, char *str, int base)
{
	/* Do this later... */
}

/* The functions:
   * memset
   * memsetw
   * memsetd
   * memcpy
   * memcmp
   * memchr

   Are implemented int stdlib.s file */
