format ELF

public memset
public memsetw
public memsetd
public memcpy
public memcmp
public memchr

section ".text"

memset:
	push edi
	mov edi, [esp + 8]
	mov edx, [esp + 12]
	mov al, dl
	shl eax, 8
	mov al, dl
	shl eax, 8
	mov al, dl
	shl eax, 8
	mov al, dl
	mov edx, [esp + 16]
	mov ecx, edx
	shr ecx, 2
	rep stosd
	mov ecx, edx
	and ecx, 3
	rep stosb
	pop edi
	ret

memsetw:
	push edi
	mov edi, [esp + 8]
	mov edx, [esp + 12]
	mov ax, dx
	shl eax, 16
	mov ax, dx
	mov edx, [esp + 16]
	mov ecx, edx
	shr ecx, 1
	rep stosd
	mov ecx, edx
	and ecx, 1
	rep stosw
	pop edi
	ret

memsetd:
	push edi
	mov edi, [esp + 8]
	mov edx, [esp + 12]
	mov eax, edx
	mov edx, [esp + 16]
	mov ecx, edx
	rep stosd
	pop edi
	ret

memcpy:
	push esi edi
	mov edi, [esp + 12]
	mov esi, [esp + 16]
	mov edx, [esp + 20]
	cmp edi, esi
	jb .reserved
	mov ecx, edx
	and ecx, 3
	rep movsb
.exit:
	pop edi esi
	ret
.reserved:
	add esi, edx
	add edi, edx
	dec esi
	dec edi
	std
	mov ecx, edx
	and ecx, 3
	rep movsb
	mov ecx, edx
	shr ecx, 2
	rep movsd
	cld
	jmp .exit

memcmp:
	push esi edi
	mov edi, [esp + 12]
	mov esi, [esp + 16]
	mov ecx, [esp + 20]
	repe cmpsb
	seta al
	setb dl
	sub al, dl
	movzx eax, al
	pop edi esi
	ret

memchr:
	push edi
	mov edi, [esp + 8]
	mov eax, [esp + 12]
	mov ecx, [esp + 16]
	repne scasb
	jne .not_found
	dec edi
	mov eax, edi
.exit:
	pop edi
	ret
.not_found:
	xor eax, eax
	jmp .exit
