#include <std/stdarg.h>
#include <std/stdio.h>
#include <std/stdint.h>

int sprintf(char *str, const char *fmt, ...)
{
	va_list list;
	va_start(list, fmt);

	vsprintf(str, fmt, list);

	va_end(list);

	return 0;
}

int vsprintf(char *str, const char *fmt, va_list args)
{
	size_t i = 0;
	while (*fmt) {
		if (*fmt == '%') {
			fmt++;
			switch (*fmt) {
				case '%':
					str[i] = '%';
					break;

				case 'd':
				//	itoa(str + i, 10);
					break;
			}
		} else
			str[i] = *fmt;
	}

	return 0;
}
