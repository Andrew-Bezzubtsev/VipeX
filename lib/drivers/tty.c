#include <std/stdint.h>
#include <std/stdlib.h>
#include <drivers/tty.h>
#include <drivers/port.h>
#include <vipex/interrupts.h>
#include <vipex/task.h>

struct tty_char_data {
	uint8_t chr;
	uint8_t attribute;
};

static uint16_t __tty_width;
static uint16_t __tty_height;
static uint16_t __tty_cursor_pos;
static uint8_t  __tty_text_attr;
static struct tty_char_data *__tty_buffer;
static uint16_t __tty_io_port;

#define READ_SCANCODE \
({ \
 	uint8_t result; \
	if (key_buffer_head != key_buffer_tail) { \
		if (key_buffer_head >= KEY_BUFFER_SIZE) \
			key_buffer_head = 0; \
\
		result = key_buffer[key_buffer_head]; \
		key_buffer_head++; \
	} else { \
		result = 0; \
	} \
\
	result; \
})

#define TTY_KEYBUF_SIZE 16
volatile static char __tty_key_buf[TTY_KEYBUF_SIZE];
volatile static unsigned __tty_key_buf_head;
volatile static unsigned __tty_key_buf_tail;

IRQ_HANDLER(keyboard_int_handler) 
{
	uint8_t code = 0, status = 0;

	if (__tty_key_buf_tail >= TTY_KEYBUF_SIZE)
		__tty_key_buf_tail = 0;

	code = port_inb(0x60);
	status = port_inb(0x61);

	__tty_key_buf_tail++;
	__tty_key_buf[__tty_key_buf_tail - 1] = code;

	port_outb(0x61, status | 1);
}

void tty_init(void)
{
	__tty_width  = *((uint16_t *)0x44A);
	__tty_height = 25;
	__tty_cursor_pos = (*((uint8_t *)0x451)) * tty_get_width() + 
		(*((uint8_t *)0x450));
	__tty_text_attr = 7;
	__tty_buffer = (struct tty_char_data *)((void *)0xB8000);
	__tty_io_port = *((uint16_t *)0x463);

	memset(__tty_key_buf, 0, TTY_KEYBUF_SIZE);
	__tty_key_buf_head = 0;
	__tty_key_buf_tail = 0;

	interrupts_set_int_handler(interrupts_get_irq_base() + 1, keyboard_int_handler, 0x8E);
}

void tty_printc(char ch)
{
	switch (ch) {
		case '\n':
			tty_set_cursor_pos((tty_get_cursor_pos() / tty_get_width() + 1) * tty_get_width());
			break;

		default:
			__tty_buffer[tty_get_cursor_pos()].chr = ch;
			__tty_buffer[tty_get_cursor_pos()].attribute = tty_get_attribute();
	}
}

void tty_prints(const char *str)
{
	while (*str) {
		tty_printc(*str);
		str++;
	}
}

char tty_readc(int wait)
{
	int shift = 0;
	uint8_t chr = 0;

	do {
		chr = READ_SCANCODE;
		switch (chr) {
		case 0x2A:
		case 0x36:
			shift = 1;
			break;

		case 0x2A + 0x80:
		case 0x36 + 0x80:
			shift = 0;
			break;

		default:
			break;
		}

		if (chr & 0x80)
			chr = 0;

		chr = get_by_scancode(chr, shift);

		if (!chr && wait)
			multitasking_switch_task();
	} while (wait && !chr);

	return chr;
}

void tty_reads(char *buf, size_t size)
{
	char chr = 0;
	size_t position = 0;

	do {
		chr = tty_readc(1);

		switch (chr) {
		case 0:
			break;

		case 8:
			if (position > 0) {
				position--;
				tty_printc(8);
			}
			break;
		
		case '\n':
			out_char('\n');
			break;

		default:
			if (position < size - 1) {
				buf[position] = chr;
				position++;
				tty_printc(chr);
			}
		}
	} while (chr != '\n');

	buf[position] = 0;
}

void tty_clear(void)
{
	memset_w(__tty_buffer, (tty_get_attribute() << 8) + ' ', tty_get_width() * tty_get_height());

	tty_set_cursor_pos(0);
}

uint16_t tty_get_width(void)
{
	return __tty_width;
}

uint16_t tty_get_height(void)
{
	return __tty_height;
}

void tty_set_attribute(uint8_t val)
{
	__tty_text_attr = val;
}

uint8_t tty_get_attribute(void)
{
	return __tty_text_attr;
}

void tty_set_cursor_pos(uint16_t pos)
{
	__tty_cursor_pos = pos;
	if (tty_get_cursor_pos() >= tty_get_width() * tty_get_height()) {
		__tty_cursor_pos = (tty_get_height() - 1) * tty_get_width();

		memcpy(__tty_buffer, __tty_buffer + tty_get_width(), 
		       tty_get_width() * tty_get_height() * sizeof(struct tty_char_data));
		memset_w(__tty_buffer + tty_get_width() * (tty_get_height() - 1),
			 (tty_get_attribute() << 8) + ' ', tty_get_width());
	}

	port_outb(__tty_io_port, 0x0E);
	port_outb(__tty_io_port + 1, tty_get_cursor_pos() >> 8);
	port_outb(__tty_io_port, 0x0F);
	port_outb(__tty_io_port + 1, tty_get_cursor_pos() & 0xFF);
}

uint16_t tty_get_cursor_pos(void)
{
	return __tty_cursor_pos;
}

#undef TTY_KEYBUF_SIZE
#undef READ_SCANCODE 
